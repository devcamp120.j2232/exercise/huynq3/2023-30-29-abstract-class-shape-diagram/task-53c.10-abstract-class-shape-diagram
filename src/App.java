import com.dev.Rectangle;
import com.dev.Triangle;


public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Rectangle rectangle1= new Rectangle("red",4,3);
        Rectangle rectangle2= new Rectangle("green",7, 5);
        System.out.println("DT rectangle1: "+rectangle1.getArea());
        System.out.println("DT rectangle2: "+rectangle2.getArea());
        System.out.println("rectangle1: "+rectangle1.toString());
        System.out.println("rectangle2: "+rectangle2.toString());

        Triangle triangle1= new Triangle("Đen", 4, 5);
        Triangle triangle2= new Triangle("Trắng", 6, 8);
        System.out.println(triangle1.getArea());
        System.out.println(triangle2.getArea());
        System.out.println(triangle1.toString());
        System.out.println(triangle2.toString());



    }
}
